<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Dummy SMS used to mock a response for example sms plugin
 *
 * @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @copyright Copyright (c) 2021 onwards The University of Nottingham
 */

require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/include/load_config.php';
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/include/custom_error_handler.inc';
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/include/autoload.inc.php';
autoloader::init();

$session = \param::required('academic_session', \param::INT, \param::FETCH_GET);
$eid = \param::optional('externalid', null, \param::TEXT, \param::FETCH_GET);
$enrols = array();

// Create some students.
$student1 = new stdClass();
$student1->id = '12345678';
$student1->username = 'student1';
$student2 = new stdClass();
$student2->id = '87654321';
$student2->username = 'student2';

if ($session == 2020) {
    // 2020.
    $enrols[] = array (
        'id' => '123',
        'name' => 'example module',
        'users' => array($student1)
    );
    $enrols[] = array (
        'id' => '456',
        'name' => 'example module 2',
        'users' => array($student2)
    );
} else {
    // Any other session.
    $enrols[] = array (
        'id' => '123',
        'name' => 'example module',
        'users' => array($student2)
    );
    $enrols[] = array (
        'id' => '456',
        'name' => 'example module 2',
        'users' => array($student1)
    );
}

// Only interested in a specific module.
if (!is_null($eid)) {
    foreach ($enrols as $e => $details) {
        if ($details[id] == $eid) {
            echo json_encode(array($details));
            exit();
        }
    }
} else {
    echo json_encode($enrols);
}


